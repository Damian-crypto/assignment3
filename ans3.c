#include <stdio.h>

int main()
{
	char c;
	printf("Enter a character: ");
	scanf("%c", &c);

	if (c == 'a' || c == 'A' ||
		c == 'e' || c == 'E' ||
		c == 'i' || c == 'I' ||
		c == 'o' || c == 'O' ||
		c == 'u' || c == 'U')
	{
		printf("You entered a vowel letter!\n");
	}
	else if (c > 64 && c < 91 || c > 96 && c < 123) // A = 65 and Z = 90, a = 97 and z = 122
	{
		printf("You entered a consonant letter!\n");
	}

	return 0;
}
