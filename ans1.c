#include <stdio.h>

int main()
{
	int number;
	printf("Enter a number: ");
	scanf("%d", &number);

	if (number == 0)
	{
		printf("Number is zero.\n");
	}
	else if (number < 0)
	{
		printf("Number is negative.\n");
	}
	else
	{
		printf("Number is positive.\n");
	}

	return 0;
}
