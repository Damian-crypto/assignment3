#include <stdio.h>

int main()
{
	int number;
	printf("Enter a number: ");
	scanf("%d", &number);

	if (number % 2 == 0)
	{
		printf("You entered an even number!\n");
	}
	else
	{
		printf("You entered an odd number!\n");
	}

	return 0;
}
